package com.newmurcery.test;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.eclipse.jetty.server.Response.OutputType;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.script.App;



public class MethodRepositary {
	
	static WebDriver driver;
	//  **static WebDriver driver = new ChromeDriver();
//	public static void appLaunch() throws InterruptedException
	//{
		/**
		 * TC_001: App Launch.
		 * @throws IOException 
		 */
	//	System.out.println("Verifying successful login with valid credentials");
		//System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
	//	//WebDriver driver = new ChromeDriver();
	//	driver.get("http://newtours.demoaut.com/");
		//Thread.sleep(3000);
		//driver.manage().window().maximize();
		//Thread.sleep(3000);
		//}
	
	public static void verifyValidCredential() throws InterruptedException, IOException
	{
		
		/**
		 * TC_002: Verifying successful login with valid credentials.
		 */
		
	    System.out.println("Verifying successful login with valid credentials");
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
		driver.manage().window().maximize();
		Thread.sleep(3000);
		WebElement uName = driver.findElement(By.name("userName"));
		uName.sendKeys("Lopa3009");
		Thread.sleep(3000);
		WebElement pword = driver.findElement(By.name("password"));
		pword.sendKeys("Lopa3009");
		WebElement submit = driver.findElement(By.xpath("//input[@value='Login']"));
		explicitWait(5,driver,submit);
		//Thread.sleep(3000);
		//WebElement submit = driver.findElement(By.name("login"));
		System.out.println("Return");
		submit.click();
		Thread.sleep(3000);
		String expectedPageTitle="Find a Flight: Mercury Tours:";
		String actualPageTitle=driver.getTitle();
		System.out.println(actualPageTitle);
		if(expectedPageTitle.equals(actualPageTitle))
				{
			System.out.println("Passed: Login Successful with Valid Credential");
				}
		else
		        {
		System.out.println("Failed: Login Unsuccessful");
		String Invalid="verifyValidCredential";
		//String verifyValidCredential;
	    NewClass.screenShot(driver, Invalid);
	}
	}
	
	public static void verifyValidInvalidLogin() throws InterruptedException, BiffException, IOException, RowsExceededException, WriteException
	
  {
		/**
		 * TC_003: Verifying successful login with invalid credentials.
		 */
		
			System.out.println("Verifying successful login with valid credentials");
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("http://newtours.demoaut.com/");
			Thread.sleep(3000);
			driver.manage().window().maximize();
			Thread.sleep(3000);
			
		String FilePath = "./TestData/Login.xls";
		String FilePath1="./TestData/TestResults.xls";
		FileInputStream fs = new FileInputStream(FilePath);
		FileOutputStream fos = new FileOutputStream(FilePath1);
		//WritableWorkbook book = Workbook.createWorkbook(new File("C:\\Selenium\\Selenium\\Output.xls")); 
		WritableWorkbook book = Workbook.createWorkbook(fos);
		
		WritableSheet sheet = book.createSheet("TestResult",0);
		//book.write();
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sh = wb.getSheet("Sheet1");
		int totalNoOfRows = sh.getRows();
		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();
		System.out.println("Row is" +totalNoOfRows);
		int col;
		//int output=totalNoOfCols+1;
		for (int row = 1; row < totalNoOfRows; row++){
			WebElement uName = driver.findElement(By.name("userName"));
			Thread.sleep(3000);
			WebElement pword = driver.findElement(By.name("password"));

			for (col = 0; col <1; col++) {
				System.out.print("Inside the loop"+"\n");
				String User=sh.getCell(col,row).getContents();
				String Password=sh.getCell(col+1,row).getContents();
				System.out.println(User);
				System.out.println(Password);

				uName.sendKeys(User);
				pword.sendKeys(Password);
			}

			Thread.sleep(3000);
			//WebElement submit = driver.findElement(By.name("login"));
			WebElement submit = driver.findElement(By.xpath("//input[@value='Login']"));
			submit.click();
			Thread.sleep(3000);
			String expectedPageTitle="Find a Flight: Mercury Tours:";
			String actualPageTitle=driver.getTitle();
			System.out.println("actual page title" + actualPageTitle);
			if(expectedPageTitle.equals(actualPageTitle))
			{
				System.out.println("Login successful");
			//	Label labTemp = new Label(2, row, "Pass");

				//Cell cell = newRow.createCell(j);
				//cell.setCellValue(dataToWrite[j])
				//sheet.addCell(labTemp);

				//driver.get("http://newtours.demoaut.com/");
				Thread.sleep(5000);
			}
			else
			{
				System.out.println("Login Unsuccessful with InValid Credential");
				//NewClass.screenShot(driver);
				//Label labTemp1 = new Label(2, row, "Fail");
				//sheet.addCell(labTemp1);
			}

		}
		//book.write();
		//book.close();
		//driver.quit();
  }

	public static void newUserRegistration() throws InterruptedException
	{
		
			System.out.println("Verifying new user registration");
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("http://newtours.demoaut.com/");
			Thread.sleep(3000);
			driver.manage().window().maximize();
			Thread.sleep(10000);
			
			//WebElement Register = driver.findElement(By.cssSelector(".mouseOver>a"));
			WebElement Register = driver.findElement(By.partialLinkText("REGIS"));
			Register.click();
			Thread.sleep(1000);
			WebElement firstName = driver.findElement(By.name("firstName"));
			firstName.sendKeys("LOPAMUDRA");
			Thread.sleep(1000);
			WebElement lastName = driver.findElement(By.name("lastName"));
			lastName.sendKeys("MUKHERJEE");
			Thread.sleep(1000);
			WebElement phone = driver.findElement(By.name("phone"));
			phone.sendKeys("9836455927");
			Thread.sleep(1000);
			WebElement email = driver.findElement(By.name("userName"));
			email.sendKeys("lopamudra3009@yahoo.com");
			Thread.sleep(1000);
			WebElement address1 = driver.findElement(By.name("address1"));
			address1.sendKeys("Thakurpukur");
			Thread.sleep(1000);
			WebElement address2 = driver.findElement(By.name("address2"));
			address2.sendKeys("Behala");
			Thread.sleep(1000);
			WebElement city = driver.findElement(By.name("city"));
			city.sendKeys("Kolkata");
			Thread.sleep(1000);
			WebElement state = driver.findElement(By.name("state"));
			state.sendKeys("WestBengal");
			Thread.sleep(1000);
			WebElement postalCode = driver.findElement(By.name("postalCode"));
			postalCode.sendKeys("700063");
			Thread.sleep(1000);
			WebElement country = driver.findElement(By.name("country"));
			Select slc=new Select(country);
			slc.selectByVisibleText("INDIA");
			Thread.sleep(1000);
			WebElement userName = driver.findElement(By.name("email"));
			userName.sendKeys("Lopa3009");
			Thread.sleep(1000);
			WebElement password = driver.findElement(By.name("password"));
			password.sendKeys("Lopa@3009");
			Thread.sleep(1000);
			WebElement confirmPassword = driver.findElement(By.name("confirmPassword"));
			confirmPassword.sendKeys("Lopa@3009");
			Thread.sleep(1000);
			WebElement submit = driver.findElement(By.xpath("//input[@name='register']"));
			submit.click();
	}
	
	public static void selectDepartingForm() throws InterruptedException, IOException
	{
		// driver = new ChromeDriver();
		WebElement passengers = driver.findElement(By.xpath("//select[@name='passCount']"));
		Select slc=new Select(passengers);
		slc.selectByVisibleText("2");
		Thread.sleep(1000);
		
		WebElement departingform = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select slc1=new Select(departingform);
		slc1.selectByVisibleText("London");
		Thread.sleep(1000);
		
		WebElement fromMonth = driver.findElement(By.xpath("//select[@name='fromMonth']"));
		Select slc2=new Select(fromMonth);
		slc2.selectByVisibleText("February");
		Thread.sleep(1000);
		
		
		WebElement fromDay = driver.findElement(By.xpath("//select[@name='fromDay']"));
		Select slc3=new Select(fromDay);
		slc3.selectByVisibleText("20");
		Thread.sleep(1000);
		
		WebElement arrivingIn = driver.findElement(By.xpath("//select[@name='toPort']"));
		Select slc4=new Select(arrivingIn);
		slc4.selectByVisibleText("London");
		Thread.sleep(1000);
		
		
		WebElement returnMonth = driver.findElement(By.xpath("//select[@name='toMonth']"));
		Select slc5=new Select(returnMonth);
		slc5.selectByVisibleText("September");
		Thread.sleep(1000);
		
		WebElement returnDay = driver.findElement(By.xpath("//select[@name='toDay']"));
		Select slc6=new Select(returnDay);
		slc6.selectByVisibleText("30");
		Thread.sleep(1000);
		
		WebElement preferences = driver.findElement(By.xpath("//input[@value='Business']"));
		preferences.click();
		Thread.sleep(1000);
		
		WebElement continue1 = driver.findElement(By.xpath("//input[@name='findFlights']"));
		continue1.click();
		Thread.sleep(1000);
	
		WebElement Verify = driver.findElement(By.cssSelector(".title>b>font"));
		String Actual=Verify.getText();
		String Expected="London to Paris";
		if(Expected.equalsIgnoreCase(Actual))
		{
		System.out.println("Cities are being selected successfully");
		}
		else
		{
		System.out.println("Cities are being selected wrong");
	//	NewClass.screenShot(driver);
		}
			
		
	}
	
	public static void excelReading() throws BiffException, IOException
	{
		String FilePath = "./TestData/Login.xls";
		FileInputStream fs = new FileInputStream(FilePath);
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sh = wb.getSheet("Sheet1");
		int totalNoOfRows = sh.getRows();

		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();


		for (int row = 1; row < totalNoOfRows; row++) {

			for (int col = 0; col < totalNoOfCols; col++) {
				System.out.print(sh.getCell(col, row).getContents() + "\t");
			
			}
			System.out.print("\n");
			}
	}
	
	public static void propertyFileReading() throws IOException
	{
		File file = new File("./TestData/Login1.properties");
		FileInputStream fileInput=new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fileInput);
		String uName=prop.getProperty("Username");
		System.out.println("Name: " + uName);
		//System.out.print("\t");
		String password=prop.getProperty("Password");
		System.out.println("Password: " + password);
		
	}
	
	public static void selectDepartingFormUsingPropertyFile() throws InterruptedException, IOException
	{
		File file = new File("./TestData/FlightSelect.properties");
		System.out.println("Inside1");
		FileInputStream fileInput=new FileInputStream(file);
		System.out.println("Inside");
		Properties prop = new Properties();
		prop.load(fileInput);
		
		String Passengers=prop.getProperty("Passengers");
		System.out.println("Inside2");
		// driver = new ChromeDriver();
		WebElement passengers = driver.findElement(By.name("passCount"));
		System.out.println("Inside3");
		Select slc=new Select(passengers);
		System.out.println("Inside4");
		slc.selectByVisibleText(Passengers);
		System.out.println("Inside5");
		Thread.sleep(1000);
		
		WebElement departingform = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select slc1=new Select(departingform);
		String Departingform=prop.getProperty("Departingform");
		slc1.selectByVisibleText(Departingform);
		Thread.sleep(1000);
		
		WebElement fromMonth = driver.findElement(By.xpath("//select[@name='fromMonth']"));
		String FromMonth=prop.getProperty("FromMonth");
		Select slc2=new Select(fromMonth);
		slc2.selectByVisibleText(FromMonth);
		Thread.sleep(1000);
		
		
		WebElement fromDay = driver.findElement(By.xpath("//select[@name='fromDay']"));
		Select slc3=new Select(fromDay);
		String FromDay=prop.getProperty("FromDay");
		slc3.selectByVisibleText(FromDay);
		Thread.sleep(1000);
		
		WebElement arrivingIn = driver.findElement(By.xpath("//select[@name='toPort']"));
		Select slc4=new Select(arrivingIn);
		String ArrivingIn=prop.getProperty("ArrivingIn");
		slc4.selectByVisibleText(ArrivingIn);
		Thread.sleep(1000);
		
		
		WebElement returnMonth = driver.findElement(By.xpath("//select[@name='toMonth']"));
		Select slc5=new Select(returnMonth);
		String ReturnMonth=prop.getProperty("ReturnMonth");
		slc5.selectByVisibleText(ReturnMonth);
		Thread.sleep(1000);
		
		WebElement returnDay = driver.findElement(By.xpath("//select[@name='toDay']"));
		Select slc6=new Select(returnDay);
		String ReturnDay=prop.getProperty("ReturnDay");
		slc6.selectByVisibleText(ReturnDay);
		Thread.sleep(1000);
		
		WebElement preferences = driver.findElement(By.xpath("//input[@value='Business']"));
		preferences.click();
		Thread.sleep(1000);
		
		WebElement continue1 = driver.findElement(By.xpath("//input[@name='findFlights']"));
		continue1.click();
		Thread.sleep(1000);
	
		WebElement Verify = driver.findElement(By.cssSelector(".title>b>font"));
		String Actual=Verify.getText();
		String Expected="London to Paris";
		if(Expected.equalsIgnoreCase(Actual))
		{
		System.out.println("Cities are being selected successfully");
		}
		else
		{
		System.out.println("Cities are being selected wrong");
		//NewClass.screenShot(driver);
		}
		
	}
	
	public static void autoItTesting() throws InterruptedException, IOException
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
		 driver = new ChromeDriver();
			driver.get("https://upload.photobox.com/en/");
			Thread.sleep(3000);
			driver.manage().window().maximize();
			Thread.sleep(3000);
			WebElement computer = driver.findElement(By.id("mycomputer"));
			computer.click();
			WebElement Browse=driver.findElement(By.id("button_desktop")) ;
			Browse.click();
			Runtime.getRuntime().exec("C:\\Selenium\\Selenium\\Tools\\Autoit\\Fileupload.exe");
			Thread.sleep(3000);
			//driver.quit();
	}
	
	public static void loginRobotClass() throws InterruptedException, AWTException, IOException
	{
		System.out.println("Verifying successful login with valid credentials");
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
		 driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		Thread. sleep(3000);
		driver.manage().window().maximize();
		Thread.sleep(3000);
		Robot rb=new Robot();
		System.out.println("Inside");
		WebElement uName = driver.findElement(By.name("userName"));
		System.out.println("Inside Username");
		//uName.click();
		//rb.keyPress(KeyEvent.VK_SHIFT+KeyEvent.VK_L);
		//rb.keyRelease(KeyEvent.VK_L);
		uName.sendKeys("Lopa3009");
		Thread.sleep(3000);
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		//WebElement pword = driver.findElement(By.name("password"));
		//pword.sendKeys("Lopa@3009");
		//rb.keyPress(KeyEvent.VK_SHIFT+KeyEvent.VK_L);
		rb.keyPress(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_L);
		//rb.keyRelease(KeyEvent.VK_SHIFT+KeyEvent.VK_L);
		rb.keyRelease(KeyEvent.VK_L);
		rb.keyRelease(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_O);
		rb.keyRelease(KeyEvent.VK_O);
		rb.keyPress(KeyEvent.VK_P);
		rb.keyRelease(KeyEvent.VK_P);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_2);
		rb.keyRelease(KeyEvent.VK_2);
		rb.keyRelease(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_3);
		rb.keyRelease(KeyEvent.VK_3);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyRelease(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyRelease(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_9);
		rb.keyRelease(KeyEvent.VK_9);
		Thread.sleep(3000);
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		//WebElement submit = driver.findElement(By.name("login"));
		//WebElement submit = driver.findElement(By.xpath("//input[@value='Login']"));
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		String expectedPageTitle="Find a Flight: Mercury Tours:";
		String actualPageTitle=driver.getTitle();
		System.out.println(actualPageTitle);
		if(expectedPageTitle.equals(actualPageTitle))
				{
			System.out.println("Passed: Login Successful with Valid Credential");
				}
		else
		        {
		System.out.println("Failed: Login Unsuccessful");
	//	NewClass.screenShot(driver);
		        }
		//driver.quit();
	}
		public static void loginActionClass() throws InterruptedException
		{
			
			/**
			 * TC_002: Verifying successful login with valid credentials.
			 */
			
		    System.out.println("Verifying successful login with valid credentials");
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
		    driver = new ChromeDriver();
		   // System.out.println("launching firefox browser"); 
			//System.setProperty("webdriver.firefox.marionette", "C:\\Selenium\\Selenium\\Tools\\geckodriver-v0.20.1-win64\\geckodriver.exe");
			driver.get("http://newtours.demoaut.com/");
			Thread.sleep(3000);
			driver.manage().window().maximize();
			Thread.sleep(3000);
			WebElement uName1 = driver.findElement(By.name("userName"));
			uName1.sendKeys("Lopa3009");
			Thread.sleep(3000);
			WebElement pword = driver.findElement(By.name("password"));
			pword.sendKeys("Lopa@3009");
			Thread.sleep(3000);
		  //WebElement submit = driver.findElement(By.name("login"));
			Actions actions = new Actions(driver);
			WebElement submit = driver.findElement(By.xpath("//input[@value='Login']"));
			actions.moveToElement(submit);
			actions.click().build().perform();
			//action.doubleClick(element).perform(); --(for double click)
			Thread.sleep(3000);
			
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));*/
			
			//driver.quit();
		
		}
		
		public static void explicitWait(int t,WebDriver driver,WebElement Element)
		{
			System.out.println("Entering"+t);
			WebDriverWait wait = new WebDriverWait(driver, t);
			wait.until(ExpectedConditions.visibilityOf(Element));
			
		}
		
		public static void siquli() throws InterruptedException, FindFailed
		{
			 System.out.println("Verifying successful login with valid credentials");
				System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
				WebDriver driver = new ChromeDriver();
				driver.get("http://newtours.demoaut.com/");
				Thread.sleep(3000);
				driver.manage().window().maximize();
				Thread.sleep(3000);
				WebElement uName = driver.findElement(By.name("userName"));
				uName.sendKeys("Lopa3009");
				Thread.sleep(3000);
				WebElement pword = driver.findElement(By.name("password"));
				pword.sendKeys("Lopa3009");	
				Screen s = new Screen();
		        Pattern fileInputTextBox = new Pattern("C:\\Selenium\\Login.PNG");
		        System.out.println(fileInputTextBox);
		        s.wait(fileInputTextBox, 5);
		        s.click(fileInputTextBox);
		}
		
		public static void webTable() throws InterruptedException
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.get("http://money.rediff.com/gainers/bsc/daily/groupa");
			Thread.sleep(3000);
			driver.manage().window().maximize();
			Thread.sleep(10000);
			 //No.of Columns
	        List <WebElement> col = driver.findElements(By.xpath(".//*[@id=\"leftcontainer\"]/table/thead/tr/th"));
	        System.out.println("No of cols are : " +col.size()); 
	      //No.of rows 
	        List <WebElement> rows = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
	        List <WebElement> price = driver.findElements(By.xpath(".//*[@id='leftcontainer']/table/tbody/tr/td[4]"));
	        System.out.println("No of rows are : " + rows.size());
	        int r=rows.size();
	        int c=col.size();	
	        String Flag="";
	        for (int i=0;i<r;i++)
	        {
	        	
	        		String BANK=rows.get(i).getText();
	        		//System.out.println("data is "+ BANK );
	        		//System.out.println("Current Price  "+ price.get(i).getText() );
	        		if(BANK.equals("State BANK"))
	        		{
	        			System.out.println(BANK);
	        			System.out.println("Current Price  "+ price.get(i).getText() );
	        			Flag="True";
	        			break;
	        		}
	        	//	else
	        		//{
	        		//Flag="False";
	        		//}
	        }	
	        	  if (Flag!="True")
	        	  {
	        		  System.out.print("BANK is not in the List");
	        	  }
	        
	        	
	        
	        
		}
		public static void frameHandling() throws InterruptedException
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Selenium\\Tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	    driver.get("http://demo.guru99.com/test/guru99home/"); 
	       // navigates to the page consisting an iframe
	    Thread.sleep(500);
	       driver.manage().window().maximize();
	      driver.switchTo().frame("a077aa5e"); //switching the frame by ID

			System.out.println("********We are switch to the iframe*******");
     		driver.findElement(By.xpath("html/body/a/img")).click();
  		    //Clicks the iframe
  		
  		     
     		int Size=driver.findElements(By.xpath("//iframe")).size();
     		System.out.println("Size is " +driver.findElements(By.xpath("//iframe")).size());
     		
     	
		}
		
		
	}
		
	
	

